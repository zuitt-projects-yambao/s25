// Aggregate count the total number of items supplied by Red Farms Inc. ($count stage)

db.fruits.aggregate([
	{
		$match: {"supplier": "Red Farms Inc."}		
	},
	{
		$count: "itemsOnRedFarms"
	}

])

// Aggregate count the total number of items with price greater than 50 ($count stage)

db.fruits.aggregate([
	{
		$match: {"price": {$gt: 50}}		
	},
	{
		$count: "noOfItemsGreaterThan50"
	}

])


// Aggregate to get the average price of all fruits that are onSale per supplier ($group)

db.fruits.aggregate([
	{
		$match: {"onSale": true}	
	},
	{
		$group: {_id: "$supplier", avgPricePerSupplier: {$avg: "$price"}}
	}

])


// Aggregate to get the highest price of fruits that are onSale per supplier. ($group)

db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id: "$supplier", maxPricePerSupplier: {$max: "$price"}}
	}

])


// Aggregate to get the lowest price of fruits that are onSale per supplier ($group)

db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id: "$supplier", minPricePerSupplier: {$min: "$price"}}
	}
])